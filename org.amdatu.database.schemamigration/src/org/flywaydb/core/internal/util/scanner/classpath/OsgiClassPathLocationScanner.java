/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.flywaydb.core.internal.util.scanner.classpath;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

/**
 * OSGi specific scanner that performs the migration search in the current bundle's classpath.
 *
 * AMDATU-JPA: Changed findResourceNames to get the bundle id from the URL and search for resources in that 
 * bundle instead of always trying to find resources in the Flyway bundle
 */
public class OsgiClassPathLocationScanner implements ClassPathLocationScanner {
	private static final Log LOG = LogFactory.getLog(OsgiClassPathLocationScanner.class);

    public Set<String> findResourceNames(String location, URL locationUrl) throws IOException {
    	Set<String> resourceNames = new TreeSet<String>();

        Bundle flywayBundle = FrameworkUtil.getBundle(Flyway.class);
        
        BundleContext bundleContext = flywayBundle.getBundleContext();
        
        String bundleID = locationUrl.getHost();
        Pattern pattern = Pattern.compile("([\\d])\\w+");
        Matcher matcher = pattern.matcher(bundleID);
        if (matcher.find()) {
        	bundleID = matcher.group(0);
        	LOG.info("Found bundleID: " + bundleID);
        }
          
		Bundle migrationBundle = bundleContext.getBundle(Double.valueOf(bundleID).intValue());
		Enumeration<URL> entries = migrationBundle.findEntries(locationUrl.getPath(), "*", true);

        if (entries != null) {
            while (entries.hasMoreElements()) {
                URL entry = entries.nextElement();
                String resourceName = getPathWithoutLeadingSlash(entry);
                LOG.info("Found OSGi resource entry: " + resourceName);
                resourceNames.add(resourceName);
            }
        }

        return resourceNames;
    }

    private String getPathWithoutLeadingSlash(URL entry) {
        final String path = entry.getPath();

        return path.startsWith("/") ? path.substring(1) : path;
    }
}
